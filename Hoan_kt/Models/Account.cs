﻿namespace Hoan_kt.Models
{
    public class Account
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string? AccountName { get; set; }
        public Customer? Customer { get; set; }
        public List<Report> Reports { get; set; }
    }
}
