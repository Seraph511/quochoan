﻿namespace Hoan_kt.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public Employee? Employee { get; set; }
        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }
        public string? Name { get; set; }

        public List<Log>? Log { get; set; }
        public ICollection<Report>? Reports { get; set; }
    }
}
