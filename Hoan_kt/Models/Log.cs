﻿namespace Hoan_kt.Models
{
    public class Log
    {
        public int Id { get; set; }
        public int TransactionalId { get; set; }
        public Transaction? Transaction { get; set; }
        public DateTime LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }
        public List<Report> Reports { get; set; }
    }
}
